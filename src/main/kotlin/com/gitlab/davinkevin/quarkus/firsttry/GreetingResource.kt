package com.gitlab.davinkevin.quarkus.firsttry

import java.time.ZonedDateTime
import java.time.ZonedDateTime.now
import javax.inject.Inject
import javax.inject.Singleton
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/")
class GreetingResource(val m: MessageService) {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun hello() = m.createMessage()
}


@Singleton
class MessageService {
    fun createMessage(): Message = Message("foo", "v1.0.0")
}

data class Message(val message: String, val version: String, val date: ZonedDateTime = now())
